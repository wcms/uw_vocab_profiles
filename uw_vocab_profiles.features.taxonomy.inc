<?php

/**
 * @file
 * uw_vocab_profiles.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_vocab_profiles_taxonomy_default_vocabularies() {
  return array(
    'uwaterloo_profiles' => array(
      'name' => 'Profile Type',
      'machine_name' => 'uwaterloo_profiles',
      'base_language' => 'und',
      'description' => 'What type of profile is this?',
      'hierarchy' => 1,
      'module' => 'uw_vocab_profiles',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
