<?php

/**
 * @file
 * uw_vocab_profiles.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_vocab_profiles_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-uwaterloo_profiles-field_term_lock'.
  $field_instances['taxonomy_term-uwaterloo_profiles-field_term_lock'] = array(
    'bundle' => 'uwaterloo_profiles',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'If a term is locked then it cannot be edited or deleted.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_lock',
    'label' => 'Term Lock',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'exclude_cv' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-uwaterloo_profiles-taxonomy_synonym'.
  $field_instances['taxonomy_term-uwaterloo_profiles-taxonomy_synonym'] = array(
    'bundle' => 'uwaterloo_profiles',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Synonyms of this term',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'ical' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'taxonomy_synonym',
    'label' => 'Synonyms',
    'required' => FALSE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'exclude_cv' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('If a term is locked then it cannot be edited or deleted.');
  t('Synonyms');
  t('Synonyms of this term');
  t('Term Lock');

  return $field_instances;
}
